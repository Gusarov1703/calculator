package ru.demaSuperCalc;

import java.util.Scanner;

 /**
  * Класс демонстрирующий работы класса SuperCalc
  *
  * @autor Gusarov Evgeniy
  */


 public class Demo {
     static Scanner scanner = new Scanner(System.in);

     public static void main(String[] args) {
         System.out.println("Напишите ваше выражение так, чтобы знак операчии окружал пробел, пример:5 + 6");
         String expression = scanner.nextLine();
         String[] symbols = expression.split(" ");
         double firstOperand = Double.valueOf(symbols[0]);
         double secondOperand = Double.valueOf(symbols[2]);

         switch (symbols[1]) {
             case "+":
                 System.out.println("сложение чисел" + SuperCalc.nAmount(firstOperand, secondOperand));
                 break;

             case "-":
                 System.out.println("вычитание" + SuperCalc.nDifference(firstOperand, secondOperand));
                 break;

             case "*":
                 System.out.println("умножение" + SuperCalc.nMultiplication(firstOperand, secondOperand));
                 break;

             case "/":
                 if (secondOperand == 0) {
                     System.out.println("деление на 0");
                 }
                 if (firstOperand % 1 == 0 || secondOperand % 1 == 0) {
                     System.out.println("целочисленное деление " + SuperCalc.nDivision((int) firstOperand, (int) secondOperand));
                 } else {
                     System.out.println("дробное деление" + SuperCalc.nDivision(firstOperand, secondOperand));
                 }
                 break;

             case "%":
                 System.out.println("остаток от деления " + SuperCalc.nRest(firstOperand, secondOperand));
                 break;

             case "^":
                 System.out.println("число в степень" + SuperCalc.nDegree(firstOperand, (int) secondOperand));
                 break;
             default:
                 break;
         }
     }
 }
